﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace TransferToInventory
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new Program();
            prog.Init();
        }

        const string InventoryPath = @"C:\Users\ayarborough\Dropbox\IT and Development\IT Inventory.xlsx";
        const string OutputPath = @"C:\Users\ayarborough\Storage\DbOutput.csv";

        // CSV string of each column in the Inventory database, in the same
        // order with the same names
        public string CsvStr = "Id,CreationDate,UpdateDate,Make,Model,PurchaseDate," +
            "ExpiryDate,SerialNumber,Name,Notes,Deleted,Processor,Memory,Diskspace," +
            "DiskType,OperatingSystem,HasCamera,ScreenSize,DisplayInterfaces_VGA," +
            "DisplayInterfaces_DVI,DisplayInterfaces_DisplayPort,DisplayInterfaces_HDMI," +
            "Discriminator,Owner_Id,Status,AssetNumber" + Environment.NewLine;

        enum Columns
        {
            ItemNum = 1,
            Department = 2,
            Description = 3,
            Lifespan = 4,
            Computer_Processor = 5,
            Computer_Memory = 6,
            Computer_Space = 7,
            Computer_OperatingSystem = 8,
            Name = 9,
            OwnerName = 10,
            Notes = 11,
            PurchaseDate = 12,
            AVG = 13,
            LogMeIn = 14,
            WindowsKey = 15,
            OfficeKey = 16,
            Checked = 17,
            LogOn = 18,
            LogOff = 19,
            Serial = 20,
            NitroProKey = 21
        }

        enum DeviceType
        {
            Desktop,
            Tablet,
            Monitor,
            Phone,
            Laptop
        }

        enum DiskType
        {
            HDD = 0,
            SSD = 1
        }

        public List<string> UsedComputerNames;
        public int StartingAssetNumber = 60;

        void Init()
        {
            Console.WriteLine("Opening excel document");

            var excelApp = new Excel.Application();
            excelApp.Visible = false;

            var workbook = excelApp.Workbooks.Open(InventoryPath);
            var worksheet = (Excel.Worksheet)workbook.Sheets[5];

            // Get the last used row in the worksheet
            var lastRowIndex = worksheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;

            Excel.Range range = worksheet.UsedRange;

            for (int rCnt = 1; rCnt <= range.Rows.Count; rCnt++)
            {
                Excel.Range cell = range.Cells[rCnt, Columns.ItemNum];
                if (cell.Value2 == null)
                    continue;

                var id = (55 + rCnt).ToString();
                var creationDate = "01/10/2010 00:00";
                var updateDate = "01/10/2015 00:00";
                var make = "-";
                var model = (string)GetValue(range.Cells[rCnt, Columns.Description].Value);
                var purchaseDate = (string)GetValue(range.Cells[rCnt, Columns.PurchaseDate].Value).ToString();
                var expiryDate = DateTime.Parse(purchaseDate).AddYears(3).ToString();
                var serialNumber = (string)GetValue(range.Cells[rCnt, Columns.Serial].Value).ToString();
                var name = (string)GetValue(range.Cells[rCnt, Columns.Name].Value).ToString();
                var notes = "";
                var deleted = "0";
                var processor = (string)GetValue(range.Cells[rCnt, Columns.Computer_Processor].Value).ToString();
                var memory = (string)GetValue(range.Cells[rCnt, Columns.Computer_Memory].Value).ToString();
                var diskSpace = (string)GetValue(range.Cells[rCnt, Columns.Computer_Space].Value).ToString();
                var diskType = DiskType.HDD.ToString();
                var operatingSystem = (string)GetValue(range.Cells[rCnt, Columns.Computer_OperatingSystem].Value).ToString();
                var hasCamera = "";
                var screenSize = "";
                var displayInterfacesVga = "";
                var displayInterfacesDvi = "";
                var displayInterfacesDisplayPort = "";
                var displayInterfacesHdmi = "";
                var discriminator = GetValue(range.Cells[rCnt, Columns.Name].Value).ToString().Contains("Laptop") ? "Laptop" : "Desktop";
                var ownerId = "40"; // ID for 'Generated' user
                var status = "0"; // Status for 'InUse' 
                var assetNumber = (StartingAssetNumber + rCnt).ToString();

                CsvStr += id + "," + creationDate + "," + updateDate + "," + make + "," + model + "," + purchaseDate + "," + expiryDate + "," + serialNumber + "," +
                    name + "," + notes + "," + deleted + "," + processor + "," + memory + "," + diskSpace + "," + diskType + "," + operatingSystem + "," + hasCamera + "," +
                    screenSize + "," + displayInterfacesVga + "," + displayInterfacesDvi + "," + displayInterfacesDisplayPort + "," +
                    displayInterfacesHdmi + "," + discriminator + "," + ownerId + "," + status + "," + assetNumber + Environment.NewLine;
            }

            System.IO.File.WriteAllText(OutputPath, CsvStr);

            Console.WriteLine("Saved!");

            workbook.Close(false, null, null);
            Marshal.FinalReleaseComObject(workbook);

            excelApp.Quit();
            Marshal.FinalReleaseComObject(excelApp);

            Console.ReadLine();
        }

        public dynamic GetValue(dynamic val)
        {
            if (val == null)
                return "-";

            Type type = val.GetType();
            Console.WriteLine(type.ToString());

            if (type == typeof(string))
            {
                string valStr = (string)val;
                if (String.IsNullOrEmpty(valStr))
                    return "-";
                else
                    return valStr;
            }
            else if (type == typeof(int?))
            {
                int? valInt = (int?)val;
                if (valInt == null)
                    return 0;
                else
                    return valInt;
            }
            else if (type == typeof(DateTime))
            {
                DateTime valDt = (DateTime)val;
                if (valDt == null)
                    return DateTime.Now;
                else
                    return valDt;
            }
            else
                return val;
        }

        private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                Console.WriteLine("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
